from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoItemForm, TodoListForm

# Create your views here.


def show_todo_lists(request):
    todolists = TodoList.objects.all()
    return render(request, "todos/list.html",
                  {"todolist_list": todolists})


def show_todo_list(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    return render(request, "todos/detail.html",
                  {"todolist": todolist})


def create_todo_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():
            todolist = form.save()
            return redirect("todo_list_detail", id=todolist.id)
    else:
        form = TodoListForm()
    context = {
        'form': form,
    }
    return render(request, "todos/create.html", context)

# EDITS TO-DO LIST NAME


def edit_todo_list(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)
        if form.is_valid():
            # To redirect to the detail view of the model, use this:
            todolist = form.save()
            return redirect("todo_list_detail", id=todolist.id)

    else:
        form = TodoListForm(instance=todolist)

    context = {"form": form}

    return render(request, "todos/edit.html", context)


def delete_todo_list(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        todolist.delete()
        return redirect("todo_list_list")

    return render(request, "todos/delete.html")

# CREATE NEW TO-DO ITEM


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todoitem = form.save()
            return redirect("todo_list_detail", todoitem.list.id)
    else:
        form = TodoItemForm()
    return render(request, "todos/create_item.html", {"form": form})


def todo_item_update(request, id):
    post = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=post)
        if form.is_valid():
            id_list = form.save()
            return redirect("todo_list_detail", id=id_list.list.id)
    else:
        form = TodoItemForm(instance=post)
    context = {
        "form": form,
        "post": post,
    }
    return render(request, "todos/update_item.html", context)
